import {OverlayRef} from '@angular/cdk/overlay';
import { Injectable, Optional, Self } from '@angular/core';

@Injectable()
export class QfToasterRef {
  constructor(
    private readonly overlay: OverlayRef
  ) {}

  close() {
    this.overlay.dispose();
  }

  isVisible() {
    return this.overlay && this.overlay.overlayElement;
  }

  getPosition() {
    return this.overlay.overlayElement.getBoundingClientRect();
  }
}
