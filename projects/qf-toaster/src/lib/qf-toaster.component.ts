
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {QfToasterAnimations, QfToasterAnimationState} from './qf-toaster.animation';
import {QFTOASTER_CONFIG_TOKEN, QfToasterConfig, QfToasterData} from './qf-toaster.config';
import {QfToasterRef} from './qf-toaster.ref';
import {AnimationEvent} from '@angular/animations';
import {QfToasterEvent} from './qf-toaster.event';


@Component({
  selector: 'qf-toaster',
  templateUrl: './qf-toaster.component.html',
 styleUrls: ['./qf-toaster.component.scss'],
  animations: [QfToasterAnimations.fadeToaster],
})
export class QfToasterComponent implements OnInit, OnDestroy {
  public animationState: QfToasterAnimationState = 'default';
  public iconType: string;
  private intervalId: any;

  constructor(
    public readonly data: QfToasterData,
    private readonly ref: QfToasterRef,
    private qfToasterEvent: QfToasterEvent,
    @Inject(QFTOASTER_CONFIG_TOKEN) public qfToasterConfig: QfToasterConfig
  ) {
    this.iconType = this.data.type === 'success' ? 'checkmark-circle-outline' : this.data.type;
  }

  ngOnInit(): void {
    this.intervalId = setTimeout(() => this.animationState = 'closing', 3000);
  }

  ngOnDestroy(): void {
   clearTimeout(this.intervalId);
  }

  public close(): void {
    this.ref.close();
  }

  onFadeComplete(event: AnimationEvent): void {
    const { toState } = event;
    const isFadeOut = (toState as QfToasterAnimationState) === 'closing';
    const isComplete = this.animationState === 'closing';
    if (isFadeOut && isComplete) {
      this.close();
      this.qfToasterEvent.closeEvent();
    }
  }
}

