import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QfToasterComponent } from './qf-toaster.component';

describe('QfToasterComponent', () => {
  let component: QfToasterComponent;
  let fixture: ComponentFixture<QfToasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QfToasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QfToasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
