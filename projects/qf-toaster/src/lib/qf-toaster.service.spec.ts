import { TestBed } from '@angular/core/testing';

import { QfToasterService } from './qf-toaster.service';

describe('QfToasterService', () => {
  let service: QfToasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QfToasterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
