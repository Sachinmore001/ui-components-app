import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QfToasterEvent {

  public onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  public closeEvent(): void {
    this.onClose.emit(false);
  }
}
