import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { Inject, Injectable, Injector, ComponentRef } from '@angular/core';
import { QfToasterComponent } from './qf-toaster.component';
import { QfToasterConfig, QfToasterData, QFTOASTER_CONFIG_TOKEN } from './qf-toaster.config';
import { QfToasterRef } from './qf-toaster.ref';

@Injectable({
  providedIn: 'root'
})
export class QfToasterService {

  private lastToaster: QfToasterRef;

  constructor(
    private overlay: Overlay,
    private parentInjector: Injector,
    @Inject(QFTOASTER_CONFIG_TOKEN) private qfToasterConfig: QfToasterConfig
  ) {}

  public show(data: QfToasterData) {
    const positionStrategy = this.getPositionStrategy();
    const overlayRef = this.overlay.create({positionStrategy});

    const qfToasterRef = new QfToasterRef(overlayRef);
    this.lastToaster = qfToasterRef;

    const injector = this.getInjector(data, qfToasterRef, this.parentInjector);
    const snackBarPortal = new ComponentPortal(QfToasterComponent, null, injector);

    overlayRef.attach(snackBarPortal);
    return qfToasterRef;
  }

  private getPositionStrategy() {
    return this.overlay.position()
      .global()
      .top(this.getPosition())
      .right(`${20}px`);
  }

  private getPosition() {
    const lastSnackbarIsVisible = this.lastToaster && this.lastToaster.isVisible();
    const position = lastSnackbarIsVisible
    ? this.lastToaster.getPosition().bottom + 10 : 20;

    return `${position}px`;
  }

  private getInjector(data: QfToasterData, qfToasterRef: QfToasterRef, parentInjector: Injector) {
    const tokens = new WeakMap();
    tokens.set(QfToasterData, data);
    tokens.set(QfToasterRef, qfToasterRef);

    return new PortalInjector(parentInjector, tokens);
  }
}
