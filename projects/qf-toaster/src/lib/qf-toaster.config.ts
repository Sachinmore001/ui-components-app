import {InjectionToken, TemplateRef, Injectable} from '@angular/core';

@Injectable()
export class QfToasterData {
  type: QfToasterType;
  text?: string;
  template?: TemplateRef<any>;
  templateContext?: {};
}

export type QfToasterType = 'success' | 'warning' | 'error' | 'info';

export interface QfToasterConfig {
  position?: {
    top: number;
    right: number;
  };
  animation?: {
    fadeIn: number;
    fadeOut: number;
  };
}

export const defaultQfToasterConfig: QfToasterConfig = {
  position: {
    top: 20,
    right: 20
  },
  animation: {
    fadeIn: 300,
    fadeOut: 300
  }
};

export const QFTOASTER_CONFIG_TOKEN = new InjectionToken<string>('qf-toaster-token');
