import { NgModule, ModuleWithProviders } from '@angular/core';
import { QfToasterComponent } from './qf-toaster.component';
import { QFTOASTER_CONFIG_TOKEN, defaultQfToasterConfig } from './qf-toaster.config';
import { OverlayModule } from '@angular/cdk/overlay';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [QfToasterComponent],
  imports: [
    OverlayModule,
    BrowserAnimationsModule
  ],
  entryComponents: [QfToasterComponent],
  exports: [QfToasterComponent]
})
export class QfToasterModule {
  public static forRoot(config = defaultQfToasterConfig): ModuleWithProviders {
    return {
      ngModule: QfToasterModule,
      providers: [
        {
          provide: QFTOASTER_CONFIG_TOKEN,
          useValue: {...defaultQfToasterConfig, ...config},
        }
      ]
    };
  }
}
