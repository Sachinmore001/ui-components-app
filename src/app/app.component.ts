import { Component, OnInit } from '@angular/core';
import { QfToasterData, QfToasterService } from 'qf-toaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ui-components-app';
  data: QfToasterData = { type: 'success', text: 'success toaster' };

  constructor(
    private qfToasterService: QfToasterService
  ) {}

  ngOnInit(): void {
    this.showToaster();
  }

  showToaster() {
    this.qfToasterService.show(this.data);
  }


}
